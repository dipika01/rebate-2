import { Injectable, Component, OnInit, Input, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import swal from 'sweetalert2';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
// import { UploadService } from '../services/upload.service';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { config } from './../../config';
import { siteConfig } from './../../assets/config';
import { SummaryComponent } from '../summary/summary.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.css']
})

// export class Appcomponent {
//   addForm: FormGroup;
//   rows: FormArray;
//   itemForm: FormGroup;

//   constructor(private fb: FormBuilder) {
//     this.addForm = this.fb.group({
//       items: [null, Validators.required],
//       items_value: ['no', Validators.required]
//     });

//   }
// }

@Injectable()
export class DynamicComponent implements OnInit {
  response = null;

  @Input() summaryof: SummaryComponent;
  id = 0;
  title = null;
  error = null;
  PageNo = 0;
  selectedFiles;
  errOfferId = false;
  errOfferDate = false;
  statusPending = false;
  statusValid = false;
  stsMsg = '';
  subId = '';
  statusDuplicate = false;
  offerCode;
  invoiceId;
  invoiceDate;
  bucketURL;

  lblSubmit
  offer;
  btnSubmit;
  entireData = [];

  products = false;
  invoiceExist = false;

  addForm: FormGroup;
  rows: FormArray;
  itemForm: FormGroup;

  urls = [];
  s3URL = [];
  invoiceInfoPage = false;

  constructor(private http: HttpClient, private router: Router, private fb: FormBuilder) {
    this.bucketURL = config.bucketURL;

    this.addForm = this.fb.group({});
    this.rows = this.fb.array([]);
  }

  ngOnInit() {
    this.response =
    {
      pages: [
        siteConfig.pages[0]
      ]
    };
    this.createForm();
    this.addForm.addControl('rows', this.rows);
    this.onAddRow();
  }

  detectFiles(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push({'url':e.target.result, 'name':file.name});
        }
        reader.readAsDataURL(file);
      }
      this.UploadToS3(files);
    }
  }

  UploadToS3(src_file){
    for(var i=0;  i< src_file.length; i++){
      var ext = src_file[i]["name"].substring(src_file[i]["name"].lastIndexOf('.') + 1, src_file[i]["name"].length);

      var dest_file = this.offerCode + '/' + this.invoiceId + "_" + src_file[i]["name"];
      this.s3URL.push(this.bucketURL + dest_file);

      const contentType = src_file[i].type;
      const bucket = new S3({
        signatureVersion: config.signatureVersion,
        accessKeyId: config.accessKeyId,
        secretAccessKey: config.secretAccessKey,
        region: config.region,
      });


      const params = {
        Bucket: config.bucket,
        Key: dest_file,
        Body: src_file[i],
        ACL: 'public-read',
        ContentType: contentType
      };
      bucket.upload(params, function (err, data) {
        if (err) {
          console.log('There was an error uploading your file: ', err);
          return false;
        }
        console.log('Successfully uploaded file.', data);
        this.s3URL.push(data.Location);
      });
    }
  }

  onAddRow() {
    this.rows.push(this.createItemFormGroup());
  }

  onRemoveRow(rowIndex: number) {
    this.rows.removeAt(rowIndex);
  }

  createItemFormGroup(): FormGroup {

    if (this.rows.value.length > 0) {
      

      return this.fb.group({
        invoiceId: this.rows.value[0].invoiceId,
        productId: null,
        quantity: null
      });
    }
    else {
      return this.fb.group({
        invoiceId: null,
        productId: null,
        quantity: null
      });
    }
  }

  invoiceImagesSubmit(serviceUrl, body) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv', 'X-Api-Key': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv' });
    let options = { headers: headers };
    let result = this.http.post(serviceUrl, body, options);
    this.entireData['invoiceImg'] = body;
    result.subscribe(
      res => {
        config.entireData = this.entireData;
        this.router.navigate(['/summary']);
      },
      err => {
        console.log("Error occured");
        this.invoiceInfoPage = false;
      }
    );

  }

  submitFrm() {

    this.errOfferId = false;
    this.errOfferDate = false;
    this.statusPending = false;
    this.statusValid = false;
    this.statusDuplicate = false;
    let productSubmitCall = false;

    var validity = (<HTMLInputElement>document.getElementById("validity")).value;
    if (validity == 'false')
      return;
    var formElement = <HTMLFormElement>document.getElementById("submitForm");
    var formData = new FormData(formElement);

    var onSuccess = { pages: [] };
    var onError = { pages: [] };
    var serviceUrl = "";

    // alert(this.response.pages.length);

    if (this.response.pages.length > 0) {
      serviceUrl = this.response.pages[0].serviceUrl;//'https://8p1q4zs910.execute-api.us-east-2.amazonaws.com/staging/offervalidate';
      for (var i = 1; i < siteConfig.pages.length; i++) {
        if (this.response.pages[0].hasOwnProperty("onSuccess")) {

          if (siteConfig.pages[i].pageId == this.response.pages[0].onSuccess) {
            onSuccess.pages.push(siteConfig.pages[i]);
            this.PageNo = i - 1;
          }
        }
        if (this.response.pages[0].hasOwnProperty("onError")) {

          if (siteConfig.pages[i].pageId == this.response.pages[0].onError) {
            this.PageNo = i - 1;
            onError.pages.push(siteConfig.pages[i]);
          }
        }
      }
    }
    if (this.response.pages[0].pageId === "productInfo") {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv', 'X-Api-Key': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv' });
      // let headers = new HttpHeaders({ 'access-control-allow-origin': '*','Content-Type': 'application/json'});
      let options = { headers: headers };
      console.log("came inside submissions")
      var body1 = this.rows.value;
      this.entireData['product'] = body1;
      console.log(body)
      //let serviceUrl = serviceUrl;
      let result = this.http.post(serviceUrl, body1, options);
      result.subscribe(
        res => {
          console.log(this.rows.value)
          // if (res == 'Invoice Id does not exist') {
          //   this.invoiceExist = true;
          //   this.response = onError;
          //   this.id = 0;
          //   this.createForm();
          //   console.log(this.invoiceExist)
          // }
          // else {
            this.invoiceExist == false;
            this.products = false;
            this.response = onSuccess;
            this.id = 0;
            this.createForm();
          // }
        },
        err => {
          console.log("Error occured");
          this.products = false;
          return false;
        }
      );
    }
    else if(this.response.pages[0].pageId === "invoiceInfo"){
      var invoiceBody = [];
      invoiceBody.push({'submissionid': this.subId});
      invoiceBody.push({'imgURL': this.s3URL});
      this.invoiceImagesSubmit(serviceUrl, invoiceBody);
    }
    else {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv', 'X-Api-Key': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv' });
      // let headers = new HttpHeaders({ 'access-control-allow-origin': '*','Content-Type': 'application/json'});
      let options = { headers: headers };


      var body = "{";
      var field = "";
      var value = "";
      var type = "";
      console.log(this.response);
      console.log(this.response.pages[0].pageId);

      for (var i = 0; i < this.response.pages[0].fields.length; i++) {
        if (this.response.pages[0].fields[i].fields) {
          if (this.response.pages[0].fields[i].fields.length > 0) {
            for (var j = 0; j < this.response.pages[0].fields[i].fields.length; j++) {
              type = this.response.pages[0].fields[i].fields[j].type;
              if (type == "string" || type == "date" || type == "file" || type == "email" || type == "number") {
                field = this.response.pages[0].fields[i].fields[j].name;
                value = this.getFieldsValue(field, type);

                if (field == 'offer') {
                  this.offerCode = value;
                }
                else if (field == 'invoiceDate') {
                  this.invoiceDate = value;
                }

                for (let i = 0; i < this.rows.length; i++) {
                  this.invoiceId = this.rows.value[0].invoiceId;
                  console.log(" inside submit ", this.invoiceId)
                }
                this.invoiceId = this.invoiceId;


                body += "\"" + field + "\":\"" + value + "\",";
              }
            }
          }
        }
        else {
          type = this.response.pages[0].fields[i].type;

          if (type == "string" || type == "date" || type == "file" || type == "email" || type == "number") {
            field = this.response.pages[0].fields[i].name;
            value = this.getFieldsValue(field, type);

            body += "\"" + field + "\":\"" + value + "\",";
          }
        }
      }
      console.log(" outsidefor ", this.invoiceId)

      if (this.response.pages[0].pageId == 'invoiceInfo')
        body += "\"submissionid\":\"" + this.subId + "\",";
      else if (this.response.pages[0].pageId == 'customerInfo') {
        body += "\"invoiceDate\":\"" + this.invoiceDate + "\",";
        body += "\"offer\":\"" + this.offerCode + "\",";
        body += "\"invoiceId\":\"" + this.invoiceId + "\",";
      }


      body = body.substring(0, body.length - 1);

      body += "}";

      //alert(body);
      if (this.response.pages[0].pageId == 'home') {
        this.entireData['offer'] = body;
      }
      else if (this.response.pages[0].pageId == 'customerInfo') {
        this.entireData['customerInfo'] = body;
      }

      let result = this.http.post(serviceUrl, body, options);
      result.subscribe(
        res => {
          console.log(res);
          var data = JSON.stringify(res);
          //console.log(JSON.parse(res['body']));
          if (res == "success") {
            config.entireData = this.entireData;
            this.router.navigate(['/summary']);
          }
          if (res == "Successful Validation") {
            this.products = true;
            this.response = onSuccess;
            if (onSuccess.hasOwnProperty("pages")) {
              if (onSuccess.pages.length > 0) {
                this.response = onSuccess;
                this.id = 0;
                this.createForm();
              }
              else {
                var resData = "Submitted Successfully!";
                if (data.hasOwnProperty("data")) {
                  if (data["data"].length > 0) {
                    resData = data["data"][0];
                  }
                  else {
                    resData = data["data"];
                  }
                }
                else {
                  resData = data;
                }
                this.title = resData;
                alert(this.title);
              }
            }
          }
          if (res == 'Offer Has closed for Submissions') {
            this.products = false;
            this.errOfferId = true;
          }
          if (res == 'Offer Date Missmatch!') {
            this.products = false;
            this.errOfferDate = true;
          }
          if (res == 'Successful Validation') {
            this.products = true;
            this.response = onSuccess;
            if (onSuccess.hasOwnProperty("pages")) {
              if (onSuccess.pages.length > 0) {
                this.response = onSuccess;
                this.id = 0;
                this.createForm();
              }
              else {
                var resData = "Submitted Successfully!";
                if (data.hasOwnProperty("data")) {
                  if (data["data"].length > 0) {
                    resData = data["data"][0];
                  }
                  else {
                    resData = data["data"];
                  }
                }
                else {
                  resData = data;
                }
                this.title = resData;
                alert(this.title);
              }
            }
          }
          if (res == "successful insertion") {
            this.products = false;
            this.response = onSuccess;
            this.id = 0;
            this.createForm();
          }


          if (res['validationStatus'] == 'pending') {
            this.products = false;
            this.statusPending = true;
            this.subId = res['submissionID'];
            this.stsMsg = res['validationStatus'];
            config.submissionId = this.subId;
            config.submissionStatus = this.stsMsg;
            this.response = onSuccess;
            this.invoiceInfoPage = true;
            this.id = 0;
            this.createForm();
          }
          else if (res['validationStatus'] == 'valid') {
            this.products = false;
            this.statusValid = true;
            this.stsMsg = res['validationStatus'];
            this.subId = res['submissionID'];
            config.submissionId = this.subId;
            config.submissionStatus = this.stsMsg;
            config.entireData = this.entireData;
            this.router.navigate(['/summary']);
          }
          else if (res['validationStatus'] == 'Duplicate') {
            this.products = false;
            this.statusDuplicate = true;
            this.stsMsg = res['validationStatus'];
            this.subId = res['submissionID'];
            config.submissionId = this.subId;
            config.submissionStatus = this.stsMsg;
            config.entireData = this.entireData;
            this.router.navigate(['/summary']);
          }
        },
        err => {
          console.log("Error occured");
          this.products = false;
          this.response = onError;

          if (onError.pages.length > 0) {
            this.response = onError;
            this.id = 0;
            this.createForm();
          }
          else {
            var resData = "Try Again Later!";
            alert(resData);
          }
        }
      );
    }
  }

  getFieldsValue(field, type = "") {
    var value = "";
    if (type == "file") {

      var file = <HTMLInputElement>document.forms["submitForm"].elements[field];

      if (file.files.length == 0) {
        return "";
      }

      var src_file = file.files[0];
      var ext = src_file["name"].substring(src_file["name"].lastIndexOf('.') + 1, src_file["name"].length);

      var dest_file = this.offerCode + '/' + this.invoiceId + "_" + src_file["name"];
      value = this.bucketURL + dest_file;

      const contentType = file.type;
      const bucket = new S3({
        signatureVersion: config.signatureVersion,
        accessKeyId: config.accessKeyId,
        secretAccessKey: config.secretAccessKey,
        region: config.region,
      });


      const params = {
        Bucket: config.bucket,
        Key: dest_file,
        Body: src_file,
        ACL: 'public-read',
        ContentType: contentType
      };
      bucket.upload(params, function (err, data) {
        if (err) {
          console.log('There was an error uploading your file: ', err);
          return false;
        }
        console.log('Successfully uploaded file.', data);
      });
    }
    else {
      var val = <HTMLInputElement>document.forms["submitForm"].elements[field];
      value = val.value;
    }
    return value;
  }
  createForm() {
    document.getElementById("myForm").innerHTML = '';

    if (this.title != null) {
      var str = "<tbody>";
      var arr = this.title.split(',');
      var name = "";
      var value = "";
      var keyVal;

      for (var i = 0; i < arr.length; i++) {
        keyVal = arr[i].split(":");
        name = keyVal[0];
        value = keyVal[1];
        if (i == 0)
          name = name.substr(2, name.length);
        else if (i == arr.length - 1)
          value = value.substr(0, value.length - 2);
        str = str + '<tr style="color:green;""><td style="text-align:left;">' + name + '</td><td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;' + value + '</td></tr>';

      }
      str = str + '</tbody>';
      if (keyVal.length > 1)
        document.getElementById("tbl").innerHTML = str;
      else
        document.getElementById("msg").innerHTML = this.title;
    }
    else if (this.error != null) {
      alert(this.error);

    }
    var id = this.id;
    for (var i = 0; i < this.response.pages[id].fields.length; i++) {
      if (this.response.pages[id].fields[i].fields) {

        if (this.response.pages[id].fields[i].fields.length > 0) {
          for (var j = 0; j < this.response.pages[id].fields[i].fields.length; j++) {
            this.createField(this.response.pages[id].fields[i].fields[j].name, this.response.pages[id].fields[i].fields[j].title, this.response.pages[id].fields[i].fields[j].type, this.response.pages[id].fields[i].fields[j].minLength, this.response.pages[id].fields[i].fields[j].maxLength, this.response.pages[id].fields[i].fields[j].required, this.response.pages[id].fields[i].fields[j].name, this.response.pages[id].fields[i].fields[j].options, this.response.pages[id].fields[i].fields[j].size, this.response.pages[id].fields[i].fields[j].src, this.response.pages[id].fields[i].fields[j].href, this.response.pages[id].fields[i].name, this.response.pages[id].fields[i].fields[j].serviceUrl);
          }
        }
      }
      else {
        this.createField(this.response.pages[id].fields[i].name, this.response.pages[id].fields[i].title, this.response.pages[id].fields[i].type, this.response.pages[id].fields[i].minLength, this.response.pages[id].fields[i].maxLength, this.response.pages[id].fields[i].required, this.response.pages[id].fields[i].name, this.response.pages[id].fields[i].options, this.response.pages[id].fields[i].size, this.response.pages[id].fields[i].src, this.response.pages[id].fields[i].href, this.response.pages[id].fields[i].serviceUrl);
      }
    }
  }
  createOptions(options, name, title) {
    for (var cnt = 0; cnt < options.length; cnt++) {
      var z = document.createElement("option");
      z.setAttribute("value", options[cnt].name);
      // z.setAttribute("value", options[cnt].value);
      // z.setAttribute("data-value", options[cnt].value);
      var t = document.createTextNode(options[cnt].name);
      z.appendChild(t);
      document.getElementById("dt" + name).appendChild(z);
    }
    var dtl = <HTMLInputElement>document.getElementById(name);
    dtl.onchange = function () {
      var optionFound = false;
      for (var j = 0; j < options.length; j++) {
        if (dtl.value == options[j].name) {
          optionFound = true;
          break;
        }
      }
      if (optionFound == false) {
        alert('Please select a valid ' + title);
        dtl.value = "";
        dtl.focus();
      }
    }
  }



  createField(name, title = null, type = 'string', minLength, maxLength, required, placeholder, options = null, size = 1, src, href, headerFooter = "", serviceUrl = "") {
    var div = document.createElement("div");
    div.setAttribute("id", "divGrp" + name);
    div.setAttribute("class", "col-md-12");
    document.getElementById("myForm").appendChild(div);

    var div1 = document.createElement("div");
    div1.setAttribute("id", "div" + name);
    div1.setAttribute("class", "form-group");
    document.getElementById("divGrp" + name).appendChild(div1);

    if (type != "img" && type != "link" && type != "heading" && type != "content" && type != "button") {
      var h = document.createElement("h6");
      if (title == null) {
        title = name;
      }
      if (required == true) {
        h.innerHTML = title + "* :";
      }
      else {
        h.innerHTML = title + " :";
      }
      document.getElementById("div" + name).appendChild(h);
    }
    if (options) {
      y = document.createElement("INPUT");

      y.setAttribute("list", "dt" + name);
      y.setAttribute("name", name);
      y.setAttribute("id", name);
      y.setAttribute("class", "form-control");
      y.setAttribute("autocomplete", "off");

      if (required)
        y.setAttribute("required", required);

      document.getElementById("div" + name).appendChild(y);
      var dtList = document.createElement("datalist");

      dtList.setAttribute("id", "dt" + name);

      document.getElementById("div" + name).appendChild(dtList);

      if (serviceUrl != "") {
        let result = this.http.get(serviceUrl);
        result.subscribe(
          res => {
            options = res;
            this.createOptions(options, name, title);
          },
          err => {
            console.log("option url err : " + JSON.stringify(err));
          }
        );
      }
      else {
        this.createOptions(options, name, title);
      }
      return;
    }

    var y;
    if (type == "string" || type == "email") {
      y = document.createElement("INPUT");

      if (type == "email")
        y.setAttribute("type", type);
      else
        y.setAttribute("type", "text");
      if (placeholder)
        y.setAttribute("Placeholder", placeholder);
      else
        y.setAttribute("Placeholder", title);

      y.setAttribute("id", name);
      y.setAttribute("name", name);
      y.setAttribute("class", "form-control");
      if (minLength)
        y.setAttribute("minLength", minLength);
      if (maxLength)
        y.setAttribute("maxLength", maxLength);
      if (required)
        y.setAttribute("required", required);


      document.getElementById("div" + name).appendChild(y);

    }
    else if (type == "content") {
      y = document.createElement("h5");
      y.setAttribute("style", "margin-top:-200px;");

      y.innerHTML = title;

      document.getElementById("div" + name).appendChild(y);
    }
    else if (type == "heading") {
      y = document.createElement("h4");

      y.innerHTML = title;
      document.getElementById("div" + name).appendChild(y);
    }
    else if (type == 'link') {
      var ul = document.getElementById(headerFooter);
      var li = document.createElement("li");
      li.setAttribute("class", "nav-item");

      y = document.createElement("a");
      y.innerHTML = title;

      y.setAttribute("id", name);
      y.setAttribute("href", href);
      y.setAttribute("data-toggle", "tab");
      y.setAttribute("role", "tab");
      y.setAttribute("aria-controls", href);
      if (name == "submit") {
        y.setAttribute("aria-selected", "true");
        y.setAttribute("class", "nav-link");
        y.setAttribute("style", "color:silver");
      }
      else {
        y.setAttribute("aria-selected", "false");
        y.setAttribute("class", "nav-link");
        y.setAttribute("style", "color:white");
      }
      li.appendChild(y);
      ul.appendChild(li);
    }
    else if (type == 'img') {
      y = document.createElement("img");
      y.setAttribute("style", "max-width: 100%;");

      if (name = "banner") {
        document.getElementById("banner").appendChild(y);
      }
      else
        document.getElementById("pageTitle").appendChild(y);
    }
    else if (type == "file") {
      y = document.createElement("INPUT");
      y.setAttribute("type", "file");
      y.setAttribute("id", name);
      y.setAttribute("name", name);
      y.setAttribute("class", "form-control");

      if (placeholder)
        y.setAttribute("Placeholder", placeholder);
      else
        y.setAttribute("Placeholder", title);

      document.getElementById("div" + name).appendChild(y);
      var file = <HTMLInputElement>document.getElementById(name);
      file.onchange = function () {
        if (file.files[0].size > (size * 1024 * 1024)) {
          alert("File must be less than " + size + " MB.");
          file.files[0] = null;
        }
      }
    }
    else if (type == "date") {
      y = document.createElement("INPUT");
      y.setAttribute("type", "date");
      y.setAttribute("class", "form-control");
      y.setAttribute("id", name);
      y.setAttribute("name", name);
      if (required)
        y.setAttribute("required", required);

      if (placeholder)
        y.setAttribute("Placeholder", placeholder);

      document.getElementById("div" + name).appendChild(y);
    }
    else if (type == "number") {
      y = document.createElement("INPUT");
      y.setAttribute("type", "number");
      y.setAttribute("class", "form-control");
      y.setAttribute("id", name);
      y.setAttribute("name", name);
      if (required)
        y.setAttribute("required", required);

      if (placeholder)
        y.setAttribute("Placeholder", placeholder);

      document.getElementById("div" + name).appendChild(y);
    }
    else if (type == "button") {
      var btn = <HTMLInputElement>document.getElementById("lbl");
      btn.innerHTML = title;
    }
  }

  // window.addEventListener("dragover",function(e){
  //   e = e || event;
  //   e.preventDefault();
  // },false);
  // window.addEventListener("drop",function(e){
  //   e = e || event;
  //   e.preventDefault();
  // },false);
  // window.addEventListener("dragenter", function(e) {
  //   if (e.target.id != 'fileElem' || e.target.id != 'drop-area') {
  //     e.preventDefault();
  //     e.dataTransfer.effectAllowed = "none";
  //     e.dataTransfer.dropEffect = "none";
  //   }
  // }, false)
  //
  // window.addEventListener("dragover", function(e) {
  //   if (e.target.id != 'fileElem' || e.target.id != 'drop-area') {
  //     e.preventDefault();
  //     e.dataTransfer.effectAllowed = "none";
  //     e.dataTransfer.dropEffect = "none";
  //   }
  // })
  //
  // window.addEventListener("drop", function(e) {
  //   if (e.target.id != 'fileElem' || e.target.id != 'drop-area') {
  //     e.preventDefault();
  //     e.dataTransfer.effectAllowed = "none";
  //     e.dataTransfer.dropEffect = "none";
  //   }
  // })

  // createFieldForProducts(name, title = null, type = 'string', minLength, maxLength, required, placeholder, options = null, size = 1, src, href, headerFooter = "") {
  //   var div = document.createElement("div");
  //   div.setAttribute("id", "divGrp" + name);
  //   div.setAttribute("class", "col-md-12");
  //   div.setAttribute("[formGroup]","addForm");
  //   document.getElementById("myForm").appendChild(div);

  //   var div2 = document.createElement("div");
  //   div2.setAttribute("id", "div" + name);
  //   div2.setAttribute("class", "form-group");
  //   document.getElementById("divGrp" + name).appendChild(div2);

  //   if (type != "img" && type != "link" && type != "heading" && type != "content" && type != "button") {
  //     var h = document.createElement("h6");
  //     if (title == null) {
  //       title = name;
  //     }
  //     if (required == true) {
  //       h.innerHTML = title + "* :";
  //     }
  //     else {
  //       h.innerHTML = title + " :";
  //     }
  //     document.getElementById("div" + name).appendChild(h);
  //   }
  //   if (options) {

  //     var x = document.createElement("SELECT");
  //     x.setAttribute("class", "form-control");
  //     x.setAttribute("id", name);
  //     document.getElementById("div" + name).appendChild(x);

  //     for (var cnt = 0; cnt < options.length; cnt++) {
  //       var z = document.createElement("option");
  //       z.setAttribute("value", options[cnt].value);

  //       var t = document.createTextNode(options[cnt].name);

  //       z.appendChild(t);

  //       document.getElementById(name).appendChild(z);
  //     }
  //     return;
  //   }

  //   var y;
  //   if (type == "content") {
  //     y = document.createElement("h5");
  //     y.setAttribute("style", "margin-top:-200px;");

  //     y.innerHTML = title;

  //     document.getElementById("div" + name).appendChild(y);
  //   }
  //   else if (type == "heading") {
  //     y = document.createElement("h4");

  //     y.innerHTML = title;
  //     document.getElementById("div" + name).appendChild(y);
  //   }
  //   else if (type == 'link') {
  //     var ul = document.getElementById(headerFooter);
  //     var li = document.createElement("li");
  //     li.setAttribute("class", "nav-item");

  //     y = document.createElement("a");
  //     y.innerHTML = title;

  //     y.setAttribute("id", name);
  //     y.setAttribute("href", href);
  //     y.setAttribute("data-toggle", "tab");
  //     y.setAttribute("role", "tab");
  //     y.setAttribute("aria-controls", href);
  //     if (name == "submit") {
  //       y.setAttribute("aria-selected", "true");
  //       y.setAttribute("class", "nav-link");
  //       y.setAttribute("style", "color:silver");
  //     }
  //     else {
  //       y.setAttribute("aria-selected", "false");
  //       y.setAttribute("class", "nav-link");
  //       y.setAttribute("style", "color:white");
  //     }
  //     li.appendChild(y);
  //     ul.appendChild(li);
  //   }
  //   else if (type == 'img') {
  //     y = document.createElement("img");
  //     y.setAttribute("src", src);

  //     if (name = "banner") {
  //       document.getElementById("banner").appendChild(y);
  //     }
  //     else
  //       document.getElementById("pageTitle").appendChild(y);
  //   }
  //   else if (type == "file") {
  //     y = document.createElement("INPUT");
  //     y.setAttribute("type", "file");
  //     y.setAttribute("id", name);
  //     y.setAttribute("name", name);
  //     y.setAttribute("class", "form-control");

  //     if (placeholder)
  //       y.setAttribute("Placeholder", placeholder);
  //     else
  //       y.setAttribute("Placeholder", title);

  //     document.getElementById("div" + name).appendChild(y);
  //     var file = <HTMLInputElement>document.getElementById(name);
  //     file.onchange = function () {
  //       if (file.files[0].size > (size * 1024 * 1024)) {
  //         alert("File must be less than " + size + " MB.");
  //         file.files[0] = null;
  //       }
  //     }
  //   }
  //   else if (type == "date") {
  //     y = document.createElement("INPUT");
  //     y.setAttribute("type", "date");
  //     y.setAttribute("class", "form-control");
  //     y.setAttribute("id", name);
  //     y.setAttribute("name", name);
  //     if (required)
  //       y.setAttribute("required", required);

  //     if (placeholder)
  //       y.setAttribute("Placeholder", placeholder);

  //     document.getElementById("div" + name).appendChild(y);
  //   }
  //   else if (type == "number") {
  //     y = document.createElement("INPUT");
  //     y.setAttribute("type", "number");
  //     y.setAttribute("class", "form-control");
  //     y.setAttribute("id", name);
  //     y.setAttribute("name", name);
  //     if (required)
  //       y.setAttribute("required", required);

  //     if (placeholder)
  //       y.setAttribute("Placeholder", placeholder);

  //     document.getElementById("div" + name).appendChild(y);
  //   }
  //   else if (type == "button") {
  //     var btn = <HTMLInputElement>document.getElementById("lbl");
  //     btn.innerHTML = title;
  //   }
  // }
}
